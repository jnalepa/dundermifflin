# DunderMifflin's repository

This repository contains the source code utilized in the following challenges organized by the European Space Agency:
- Collision Avoidance Challenge (we were ranked 7th across 97 participating teams): https://kelvins.esa.int/collision-avoidance-challenge/

