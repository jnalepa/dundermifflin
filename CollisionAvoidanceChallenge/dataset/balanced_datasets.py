import copy
import os
import pickle
import typing
from random import shuffle

import joblib
import numpy as np
import pandas as pd
import torch
from sklearn.preprocessing import StandardScaler
from torch.utils import data

RISK = 'risk'
EVENT_ID = 'event_id'
C_OBJECT_TYPE = 'c_object_type'
MISSION_ID = 'mission_id'
TIME_TO_TCA = 'time_to_tca'

SHARED_COLUMNS = [
    'x_sigma_rdot', 'x_sigma_n', 'x_cn_r', 'x_cn_t', 'x_cndot_n',
    'x_sigma_ndot', 'x_cndot_r', 'x_cndot_rdot',
    'x_cndot_t', 'x_cndot_tdot', 'x_sigma_r', 'x_ct_r', 'x_sigma_t',
    'x_ctdot_n', 'x_crdot_n', 'x_crdot_t',
    'x_crdot_r', 'x_ctdot_r', 'x_ctdot_rdot', 'x_ctdot_t', 'x_sigma_tdot',
    'x_position_covariance_det',
    'x_cd_area_over_mass', 'x_cr_area_over_mass', 'x_h_apo', 'x_h_per',
    'x_j2k_ecc', 'x_j2k_inc', 'x_j2k_sma',
    'x_sedr', 'x_span', 'x_rcs_estimate', 'x_actual_od_span',
    'x_obs_available', 'x_obs_used',
    'x_recommended_od_span', 'x_residuals_accepted', 'x_time_lastob_end',
    'x_time_lastob_start', 'x_weighted_rms'
]

UNIQUE_COLUMNS = [
    EVENT_ID, RISK, 'time_to_tca', 'max_risk_estimate', 'max_risk_scaling',
    'miss_distance', 'relative_speed',
    'relative_position_n', 'relative_position_r', 'relative_position_t',
    'relative_velocity_n',
    'relative_velocity_r', 'relative_velocity_t', 'c_object_type',
    'geocentric_latitude', 'azimuth',
    'elevation', 'F10', 'AP', 'F3M', 'SSN'
]


def create_x_df(df: pd.DataFrame, path: str):
    df.drop(columns=[MISSION_ID], inplace=True)
    df = df.fillna(df.mean())
    columns = {}
    for key in UNIQUE_COLUMNS:
        columns[key] = df[key]
    columns.update(
        {key: (df[key.replace('x', 't')] + df[key.replace('x', 'c')]) for key
         in SHARED_COLUMNS})
    out_df = pd.concat(columns, axis=1)
    out_df = pd.get_dummies(out_df, prefix=[C_OBJECT_TYPE])
    out_df.to_csv(os.path.join(path, 'data_x_corrected.csv'), index=False)


def create_tc_df(df: pd.DataFrame, path: str):
    df.drop(columns=[MISSION_ID], inplace=True)
    df = df.fillna(df.mean())
    out_df = pd.get_dummies(df, prefix=[C_OBJECT_TYPE])
    out_df.to_csv(os.path.join(path, 'data_tc_corrected.csv'), index=False)


def generate_samples(destination_directory: str):
    df = pd.read_csv(
        os.path.join(destination_directory, 'data_x_corrected.csv'))
    train_samples = [[] for _ in range(6)]
    test_samples = []
    for event in df['event_id'].unique():
        sample = df[df['event_id'] == event]
        last_risk = sample['risk'].to_numpy()[-1]
        last_tca = sample['time_to_tca'].to_numpy()[-1]
        first_tca = sample['time_to_tca'].to_numpy()[0]
        if 0 > last_risk >= -5 and last_tca < 1 and first_tca > 2:
            train_samples[0].append(sample)
        if -5 > last_risk >= -10 and last_tca < 1 and first_tca > 2 and \
                sample.to_numpy().shape[0] > 10:
            train_samples[1].append(sample)
        if -10 > last_risk >= -15 and last_tca < 1 and first_tca > 2 and \
                sample.to_numpy().shape[0] > 10:
            train_samples[2].append(sample)
        if -15 > last_risk >= -20 and last_tca < 1 and first_tca > 2 and \
                sample.to_numpy().shape[0] > 10:
            train_samples[3].append(sample)
        if -20 > last_risk >= -25 and last_tca < 1 and first_tca > 2 and \
                sample.to_numpy().shape[0] > 10:
            train_samples[4].append(sample)
        if -25 > last_risk >= -30 and last_tca < 1 and first_tca > 2 and \
                sample.to_numpy().shape[0] > 10:
            train_samples[5].append(sample)
        if 0 <= last_tca <= 1 and \
                sample[sample['time_to_tca'] >= 2].to_numpy().shape[0] > 0:
            up_2days_sample = sample[sample['time_to_tca'] >= 2]
            new_sample = pd.concat(
                [up_2days_sample, pd.DataFrame(sample.iloc[-1]).transpose()],
                axis=0)
            test_samples.append(new_sample)

    min_len = len(train_samples[0])
    [shuffle(item) for item in train_samples]
    train_samples = [item[:min_len] for item in train_samples]

    train_samples = [item for sublist in train_samples for item in sublist]

    shuffle(train_samples)
    shuffle(test_samples)

    with open(os.path.join(destination_directory, 'train_samples_x.pickle'),
              'wb') as file:
        pickle.dump(train_samples, file)
    with open(os.path.join(destination_directory, 'test_samples_x.pickle'),
              'wb') as file:
        pickle.dump(test_samples, file)


class Sample(object):
    def __init__(self, df: pd.DataFrame):
        self.features = df.drop(columns=[EVENT_ID, RISK]).to_numpy().astype(
            np.float64)
        self.risk = np.expand_dims(df[RISK].to_numpy(), -1).astype(np.float64)
        self.event_id = df[EVENT_ID].to_numpy().astype(np.int)[0]
        self.time_to_tca_id = df.drop(
            columns=[EVENT_ID, RISK]).columns.get_loc(TIME_TO_TCA)

    def get_tensors(self) -> typing.Tuple[torch.Tensor, torch.Tensor, int]:
        features = torch.unsqueeze(torch.from_numpy(self.features),
                                   dim=0).type(torch.FloatTensor)
        risk = torch.from_numpy(self.risk).squeeze().type(torch.FloatTensor)
        return features, risk, self.event_id


class KelvinsDataset(data.Dataset):
    RISK_MIN = -30

    def __init__(self, samples: typing.List[pd.DataFrame], scaler_path: str):
        scaler = joblib.load(scaler_path)
        self.scaler = scaler['scaler']
        self.scaler_columns = scaler['columns']

        for sample in samples:
            assert sample.drop(columns=['event_id',
                                        'risk']).columns.tolist() == self.scaler_columns

        self.samples = [Sample(df) for df in samples]

    def __len__(self) -> int:
        return int(len(self.samples))

    def __getitem__(self, item) -> Sample:
        sample = copy.copy(self.samples[item])
        # Return normalized copy of sample:
        sample.features = self.scaler.transform(sample.features)
        # sample.risk = (sample.risk - self.RISK_MIN) / (0 - self.RISK_MIN)
        return sample

    def risk_inverse_transform(self, risk: np.ndarray) -> np.ndarray:
        return risk * (0 - self.RISK_MIN) + self.RISK_MIN


# Generate csv file, if done once comment out:
df = pd.read_csv('')
create_tc_df(df, '')

generate_samples(destination_directory='')
with open('', "rb") as file:
    tr = pickle.load(file)
with open('', "rb") as file:
    test = pickle.load(file)
dataset = KelvinsDataset(tr, StandardScaler())
sample = dataset[2]
features, risk, event_id = sample.get_tensors()
inverse_risk = dataset.scaler.risk_inverse_transform(risk.numpy())
