import os
import pickle
from random import shuffle

import joblib
import pandas as pd
from sklearn import preprocessing

SHARED_COLUMNS = [
    'x_sigma_rdot', 'x_sigma_n', 'x_cn_r', 'x_cn_t', 'x_cndot_n',
    'x_sigma_ndot', 'x_cndot_r', 'x_cndot_rdot',
    'x_cndot_t', 'x_cndot_tdot', 'x_sigma_r', 'x_ct_r', 'x_sigma_t',
    'x_ctdot_n', 'x_crdot_n', 'x_crdot_t',
    'x_crdot_r', 'x_ctdot_r', 'x_ctdot_rdot', 'x_ctdot_t', 'x_sigma_tdot',
    'x_position_covariance_det',
    'x_cd_area_over_mass', 'x_cr_area_over_mass', 'x_h_apo', 'x_h_per',
    'x_j2k_ecc', 'x_j2k_inc', 'x_j2k_sma',
    'x_sedr', 'x_span', 'x_rcs_estimate', 'x_actual_od_span',
    'x_obs_available', 'x_obs_used',
    'x_recommended_od_span', 'x_residuals_accepted', 'x_time_lastob_end',
    'x_time_lastob_start', 'x_weighted_rms'
]

UNIQUE_COLUMNS = [
    'event_id', 'risk', 'time_to_tca', 'max_risk_estimate', 'max_risk_scaling',
    'miss_distance', 'relative_speed',
    'relative_position_n', 'relative_position_r', 'relative_position_t',
    'relative_velocity_n',
    'relative_velocity_r', 'relative_velocity_t', 'c_object_type',
    'geocentric_latitude', 'azimuth',
    'elevation', 'F10', 'AP', 'F3M', 'SSN'
]


def create_df(df_path: str, file_name: str, mode: str):
    df = pd.read_csv(df_path)
    df.drop(columns=['mission_id'], inplace=True)
    df = df.fillna(df.mean())
    if mode.lower() == 'x':
        columns = {}
        for key in UNIQUE_COLUMNS:
            columns[key] = df[key]
        columns.update(
            {key: (df[key.replace('x', 't')] + df[key.replace('x', 'c')]) for
             key in SHARED_COLUMNS})
        df = pd.concat(columns, axis=1)
    df = pd.get_dummies(df, prefix=['c_object_type'])
    df.to_csv(os.path.join(os.path.dirname(df_path), file_name), index=False)


def generate_test_set(df_path: str, destination_dir: str, file_name: str):
    df = pd.read_csv(df_path)
    test_samples = []
    for event in df['event_id'].unique():
        sample = df[df['event_id'] == event]
        last_tca = sample['time_to_tca'].to_numpy()[-1]

        if 0 <= last_tca <= 1 and \
                sample[sample['time_to_tca'] >= 2].to_numpy().shape[0] > 0:
            up_2days_sample = sample[sample['time_to_tca'] >= 2]
            new_sample = pd.concat(
                [up_2days_sample, pd.DataFrame(sample.iloc[-1]).transpose()],
                axis=0, ignore_index=True)
            test_samples.append(new_sample)

    shuffle(test_samples)

    with open(os.path.join(destination_dir, file_name), 'wb') as file:
        pickle.dump(test_samples, file)


def generate_train_set(df_path: str, destination_dir: str, file_name: str,
                       n_ranges: int):
    df = pd.read_csv(df_path)
    train_samples = [[] for _ in range(n_ranges)]

    for event in df['event_id'].unique():
        sample = df[df['event_id'] == event].reset_index(drop=True)
        last_risk = sample['risk'].to_numpy()[-1]
        first_tca, last_tca = sample['time_to_tca'].to_numpy()[0], \
                              sample['time_to_tca'].to_numpy()[-1]

        if n_ranges == 6:
            if 0 >= last_risk >= -5 and last_tca < 1 and first_tca > 2:
                train_samples[0].append(sample)
            if -5 > last_risk >= -10 and last_tca < 1 and first_tca > 2 and \
                    sample.to_numpy().shape[0] > 10:
                train_samples[1].append(sample)
            if -10 > last_risk >= -15 and last_tca < 1 and first_tca > 2 and \
                    sample.to_numpy().shape[0] > 10:
                train_samples[2].append(sample)
            if -15 > last_risk >= -20 and last_tca < 1 and first_tca > 2 and \
                    sample.to_numpy().shape[0] > 10:
                train_samples[3].append(sample)
            if -20 > last_risk >= -25 and last_tca < 1 and first_tca > 2 and \
                    sample.to_numpy().shape[0] > 10:
                train_samples[4].append(sample)
            if -25 > last_risk >= -30 and last_tca < 1 and first_tca > 2 and \
                    sample.to_numpy().shape[0] > 10:
                train_samples[5].append(sample)

        elif n_ranges == 2:
            if last_risk >= -6 and last_tca < 1 and first_tca > 2:
                train_samples[0].append(sample)
            if last_risk < -6 and last_tca < 1 and first_tca > 2 and \
                    sample.to_numpy().shape[0] > 10:
                train_samples[1].append(sample)
        else:
            raise ValueError('The passed number of ranges is not correct.')

    min_len = len(train_samples[0])

    [shuffle(item) for item in train_samples]

    train_samples = [item[:min_len] for item in train_samples]

    [print('Length of patch: {}'.format(len(item))) for item in train_samples]

    train_samples = [item for sublist in train_samples for item in sublist]

    shuffle(train_samples)

    with open(os.path.join(destination_dir, file_name), 'wb') as file:
        pickle.dump(train_samples, file)


def save_scaler(df_path: str, destination_path: str, file_name: str):
    df = pd.read_csv(df_path).drop(columns=['event_id', 'risk'])
    model = preprocessing.MinMaxScaler().fit(df)
    time_to_tca_id = df.columns.get_loc('time_to_tca')
    model.data_min_[time_to_tca_id] = 0
    columns = df.columns.tolist()
    joblib.dump({'scaler': model, 'columns': columns},
                os.path.join(destination_path, file_name))


create_df('', 'test_data_tc.csv', 'tc')
generate_train_set('', '', 'train_samples_tc_no_augmentation.pickle',
                   n_ranges=6)
generate_test_set('', '', 'val_samples_x.pickle')
with open('', 'rb') as f:
    samples = pickle.load(f)
save_scaler('', '', 'x_scaler.pickle')
k = joblib.load('')
