from typing import List

import numpy as np
import pandas as pd
import torch
from sklearn.preprocessing import MinMaxScaler
from torch.utils.data import Dataset


class KelvinsDataset(Dataset):
    def __init__(self, path_to_csv: str, columns_to_drop=None,
                 na_fill: str = 'mean', device='cpu'):
        """
        Representation of the Kelvins Collision Avoidance Challenge.
        :param path_to_csv: Path to the .csv training file
        :param columns_to_drop: Name of the columns to drop, default column
        to drop is 'mission_id'
        :param na_fill: What method to use when inserting NaN values. Default
        is 'mean'
        :param device: Whether to allocate tensors on cpu or gpu
        """
        if columns_to_drop is None:
            columns_to_drop = ['mission_id']
        self.min_max_scaler = MinMaxScaler()
        data = self._preprocess_data(pd.read_csv(path_to_csv),
                                     columns_to_drop, na_fill)
        self.data = self._construct_dataset(data)
        self.device = device

    def _preprocess_data(self, data: pd.DataFrame, columns_to_drop: List,
                         na_fill: str):
        """
        Preprocess the data, i.e standardize, remove unnecessary columns,
        one-hot-encode categorical columns and fill na values.
        :param data: Data to preprocess
        :param columns_to_drop: Names of columns to drop
        :param na_fill: Method to use when inserting NaN values
        :return: Preprocessed data
        """
        data = data.drop(columns=columns_to_drop)
        data = pd.get_dummies(data, prefix=['c_object_type'])
        if na_fill == 'mean':
            data = data.fillna(data.mean())
        else:
            data = data.fillna(0)
        data_scaled = self.min_max_scaler.fit_transform(
            data.drop(columns='event_id').values)
        return pd.concat([data['event_id'],
                          pd.DataFrame(data_scaled, columns=data.columns[1:])],
                         axis=1)

    @staticmethod
    def _construct_dataset(data: pd.DataFrame):
        """
        Organize the dataset into unique events with their respective cdms and
        risks
        :param data: Data from which the dataset will be built
        :return: Constructed data
        """
        unique_events = data['event_id'].unique()
        container = list()
        for event_id in unique_events:
            event_data = data.loc[data['event_id'] == event_id]
            event_data = event_data.drop(columns='event_id')
            container.append((np.array(event_data, dtype=np.float32),
                              np.array(event_data['risk'], dtype=np.float32)))
        return container

    def __getitem__(self, item):
        """
        Return sequence and risk at specified index
        :param item: Index of the item to return
        :return: Sequence (sequence_length x number_of_features),
        risk (sequence_length)
        """
        return torch.tensor(self.data[item][0],
                            device=self.device).float(), torch.tensor(
            self.data[item][1], device=self.device).float()

    def __len__(self):
        """
        :return: Number of sequences in the dataset
        """
        return len(self.data)

    @staticmethod
    def custom_collate(batch: list) -> tuple:
        """
        Puts each data field into a tensor with outer dimension batch size
        :param batch: List of samples
        :return: Torch tensor
        """
        # Other type of preprocessing the input batch:
        # max_dim = max([elem[0].numpy().shape[0] for elem in batch])
        # time_steps = []
        # for elem in batch:
        #     pad_size = max_dim - elem[0].shape[0]
        #     time_steps.append(int(elem[0].shape[0]))
        #     elem[0] = functional.pad(elem[0], (0, 0, 0, pad_size), 'constant', 0)
        #     elem[1] = functional.pad(elem[1], (0, pad_size), 'constant', 0)
        # samples = torch.stack([elem[0] for elem in batch], 0)
        # targets = torch.stack([elem[1] for elem in batch], 0)
        return [elem[0] for elem in batch], [elem[1] for elem in batch]
