import operator
import os
import pickle
import random
from random import shuffle

import numpy as np
import pandas as pd
from imblearn import over_sampling


class Augmenter(object):
    def __init__(self, samples_path: str):
        self.samples = None
        with open(samples_path, 'rb') as file:
            self.samples = pickle.load(file)
        self.dir_name = os.path.dirname(samples_path)
        assert self.samples is not None

    def gaussian_noise_augmentation(self, file_name: str, n_times: int,
                                    lambda_: float):
        new_samples = []
        for index in range(len(self.samples)):
            event = self.samples[index]
            new_events = []

            for i in range(n_times):
                operator_ = random.choice((operator.add, operator.sub))

                data = operator_(
                    event.drop(columns=['event_id']).to_numpy().copy(),
                    (lambda_ * np.random.normal(
                        np.mean(event.drop(columns=['event_id']).to_numpy(),
                                axis=0),
                        np.std(event.drop(columns=['event_id']).to_numpy(),
                               axis=0))))

                new_event = pd.DataFrame(columns=event.columns,
                                         data=np.hstack((np.expand_dims(
                                             -event['event_id'].to_numpy(),
                                             axis=-1), data)))
                new_events.append(new_event)

            new_samples.extend(new_events)

        new_samples += self.samples
        shuffle(new_samples)
        with open(os.path.join(self.dir_name, file_name), 'wb') as file:
            pickle.dump(new_samples, file)

    def smote_augmentation(self, file_name: str):
        smote = over_sampling.SMOTENC()
        for index in range(len(self.samples)):
            sample = self.samples[index]
            new_sample = smote.fit_resample(
                sample.drop(columns=['event_id']).to_numpy().copy(),
                np.random.randint(size=(sample.shape[0]), low=0, high=3))


augmenter = Augmenter('')
augmenter.smote_augmentation('')
